FROM jamesandariese/xpra-base

RUN apt-get update && \
    apt-get install -y chromium && \
    rm -rf /var/lib/apt/lists/*

RUN adduser --disabled-password user --gecos "User"
USER user
ENV HOME /home/user

VOLUME ["/home/user"]

# don't forget to create your /password file

CMD ["--start-child=chromium --no-sandbox"]
